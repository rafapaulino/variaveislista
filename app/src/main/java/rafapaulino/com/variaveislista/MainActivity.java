package rafapaulino.com.variaveislista;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    int numeroInteiro = 0;
    float numerosReais = 0.55f;
    boolean estado = false;
    String palavra = "Nome";

    int listaNumeros[] = {1, 2, 3, 4, 5, 6};
    String listaNomes[] = {"Android","PHP","Java","ObjectiveC"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        System.out.println(numeroInteiro);
        System.out.println(numerosReais);
        System.out.println(estado);
        System.out.println(palavra);
        System.out.println(listaNumeros);
        System.out.println(listaNomes);

    }
}
